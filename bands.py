from constants import IMAGE_QUALITY_STRINGS
from constants import QA_STRING
from constants import MTL_STRING
from identifiers import LANDSAT_BANDS
from identifiers import LEVEL_2_IMAGES
from identifiers import LANDSAT_IDENTIFIERS
from messages import MESSAGE_UNKNOWN_LANDSAT_IDENTIFIER
import os
import glob
import re
import grass.script as grass
from identify import identify_product_collection
from identify import identify_product_level


def find_existing_band(band):
    """
    Check if band exists in the current mapset

    Parameter "element": 'raster', 'raster_3d', 'vector'
    """
    result = grass.find_file(name=band, element='cell', mapset='.')
    if result['file']:
        # grass.verbose(_("Band {band} exists".format(band=band)))
        return True
    else:
        return False


def list_product_bands(scene):
    """
    List bands (images) of a scene/product

    Parameters
    ----------

    scene :
        A Landsat product identifier string

    Returns
    -------
    All bands/images integer numbers (i.e. [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 'QA'] or even ['SR_B1', 'SR_B2', ...])
    """
    product_collection = identify_product_collection(os.path.basename(scene))
    if product_collection == 'Collection 1':
        bands = LANDSAT_BANDS['all']
    elif product_collection == 'Collection 2':
        product_level = identify_product_level(os.path.basename(scene))
        bands = list(LEVEL_2_IMAGES.keys())
    return bands


def match_band_filenames(bands, scene):
    """
    Retrieve filenames of user requested bands from a Landsat scene

    To Do
    -----
    Fix: requires a fix for 'tar.gz' files, i.e. if 'scene' = '*.tar.gz'!

    Parameters
    ----------
    bands :
        List of user requested bands

    scene :
        Landsat scene directory

    Returns
    -------
        Returns list of filenames of user requested bands

    Example
    -------
        ...
    """
    try:
        product_collection = identify_product_collection(os.path.basename(scene))
        if product_collection == 'Collection 1':
            regular_expression_template = LANDSAT_IDENTIFIERS['band_template'][product_collection]
        elif product_collection == 'Collection 2':
            product_level = identify_product_level(os.path.basename(scene))
            regular_expression_template = LANDSAT_IDENTIFIERS['band_template'][product_collection][product_level]
            bands = list(LEVEL_2_IMAGES.keys())
    except:
        grass.fatal(_(MESSAGE_UNKNOWN_LANDSAT_IDENTIFIER.format(scene=scene)))
    requested_filenames = []
    for band in bands:
        for filename in os.listdir(scene):
            template = regular_expression_template.format(band_pattern=band)
            pattern = re.compile(template)
            if pattern.match(filename):
                absolute_filename = scene + '/' + filename
                requested_filename = os.path.basename(glob.glob(absolute_filename)[0])
                requested_filenames.append(requested_filename)
    return sort_band_filenames(requested_filenames)

def retrieve_band_filenames(
        bands,
        spectral_sets,
        scene,
    ):
    """
    """
    # This will fail if the 'scene=' is a compressed one, i.e. tar.gz # FIXME
    if bands == [''] and spectral_sets == ['']:
        spectral_sets = ['all']

    if not spectral_sets == ['']:
        band_subset = list_band_sets(
                        spectral_sets,
                        scene,
                    )
        bands.extend(band_subset)

    band_filenames = match_band_filenames(
                bands=bands,
                scene=scene,
            )
    return band_filenames

def list_band_sets(spectral_sets, scene):
    """
    """
    requested_bands = []
    for spectral_set in spectral_sets:
        bands = list(LANDSAT_BANDS[spectral_set])
        requested_bands.extend(bands)
    return list(set(requested_bands))

def sort_band_filenames(band_filenames):
    """
    """
    filenames = sorted(band_filenames, key=lambda item:
            (int(item.partition('_B')[2].partition('.')[0])
                if item.partition('_B')[2].partition('.')[0].isdigit()
                else float('inf'), item))
    return filenames

def get_name_band(scene, filename, single_mapset=False):
    """
    """
    absolute_filename = os.path.join(scene, filename)

    # found a wrongly named *MTL.TIF file in LE71610432005160ASN00
    if MTL_STRING in absolute_filename:  # use grass.warning(_("..."))?
        message_fatal = "Detected an MTL file with the .TIF extension!"
        message_fatal += "\nPlease, rename the extension to .txt and retry."
        grass.fatal(_(message_fatal))

    # is it a level 2 product band/image name?
    if identify_product_level(scene) == 'Level 2':
        name = '_'.join(os.path.splitext(filename)[0].split('_')[-2:])

    # keep only the last part of the filename
    else:
        name = os.path.splitext(filename)[0].rsplit('_')[-1]
    # if any(string in absolute_filename for string in LEVEL_2_IMAGES.keys()):
    #     band = name
    # detect image quality strings in filenames
    # source: https://stackoverflow.com/q/7351744/1172302
    if any(string in absolute_filename for string in IMAGE_QUALITY_STRINGS):
        # name = 'QA_' + name
        band = name
    if (QA_STRING) in absolute_filename:
        band = name

    # is it a two-digit multispectral band?
    elif len(name) == 3 and name[0] == 'B' and name[-1] == '0':
        band = int(name[1:3])

    # what is this for?
    elif len(name) == 3 and name[-1] == '0':
        band = int(name[1:2])

    # what is this for?
    elif len(name) == 3 and name[-1] != '0':
        band = int(name[1:3])

    # is it a single-digit band?
    else:
        if re.search(r'\d+', name):
            band = int(re.search(r'\d+', name).group())
        else:
            band = name

    # one Mapset requested? prefix raster map names with scene id
    if single_mapset:
        name = os.path.basename(scene) + '_' + name

    return name, band
