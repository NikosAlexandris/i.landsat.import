# Following the order of Landsat Identifiers

# scene and product identifiers, regular expression patterns and templates

# Collection 1, Level 1 product identifier: LXSS_LLLL_PPPRRR_YYYYMMDD_yyyymmdd_CC_TX
# Where:
#     L = Landsat
#     X = Sensor (“C”=OLI/TIRS combined, “O”=OLI-only, “T”=TIRS-only, “E”=ETM+, “T”=“TM, “M”=MSS)
#     SS = Satellite (”07”=Landsat 7, “08”=Landsat 8)
#     LL[LL] = Processing correction level ("L1"=Level 1, "L2"=Level 2)
#     [LL]LL = Processing correction software (L1TP/L1GT/L1GS)
#     PPP = WRS path
#     RRR = WRS row
#     YYYYMMDD = Acquisition year, month, day
#     yyyymmdd - Processing year, month, day
#     CC = Collection number (01, 02, …)
#     TX = Collection category (“RT”=Real-Time, “T1”=Tier 1, “T2”=Tier 2)

# LANDSAT_PRODUCT_IDENTIFIER = {
#         'PREFIX': 0,
#         'SENSOR': 1,
#         'SATELLITE': {'START': 2, 'END': 3},
#         'PROCESSING_CORRECTION_LEVEL': {'START': 5, 'END': 9},
#         'PATH': {'START': 10, 'END': 13},
#         'ROW': {'START': 13, 'END': 16},
#         'ACQUISITION_DATE': {'START': 17, 'END': 25},
#         'PROCESSING_DATE': {'START': 26, 'END': 34},
#         'COLLECTION_NUMBER': {'START': 35, 'END': 37},
#         'COLLECTION_CATEGORY': {'START': 38, 'END': 40},
#         }

DELIMITER = '_'
DELIMITER_RE = '(?P<delimiter>_)'
DELIMITER_RE_GROUP = '(?P=delimiter)'
LANDSAT_PREFIX = 'L'
LANDSAT_PREFIX_RE = '(?P<prefix>{prefix})'.format(prefix=LANDSAT_PREFIX)
LANDSAT_PREFIX_RE_GROUP = '(?P<prefix>L)'


SENSOR = {
        'identifiers': ('C', 'T', 'E', 'M'),
        'regular_expression': {
            'Pre-Collection': '(?P<sensor>[C|T|M|S])',
            'Collection 1': '(?P<sensor>[C|O|T|E|S])',
            'Collection 2': '(?P<sensor>[C|O|T|E|S])',
            }
        }
SENSORS = {
        'C': 'OLI/TIRS',
        'O': 'OLI',
        'T': 'TIRS',
        'E': 'ETM+',
        'M': 'TM',
        'S': 'MSS'
        }
SENSOR_RE = '(?P<sensor>[C|O|T|E|S])'
# Pre-collection -- Obsolete!
SENSORS_PRECOLLECTION = {
        'C': 'OLI/TIRS',
        'E': 'ETM+',
        'M': 'TM',
        'S': 'MSS'
        }
SENSOR_PRECOLLECTION_RE = '(?P<sensor>[C|E|M|S])'
# FIXME: Below: T for TIRS and T for TM!
SENSORS_COLLECTION_1 = {
        'C': 'OLI/TIRS',
        'O': 'OLI',
        'T': 'TIRS',
        'E': 'ETM+',
        'M': 'TM',
        'S': 'MSS'
        }
SENSOR_COLLECTION_1_RE = '(?P<sensor>[C|O|T|E|M|S])'
SENSORS_COLLECTION_2 = {
        'C': 'OLI/TIRS',
        'O': 'OLI',
        'T': 'TIRS',
        'E': 'ETM+',
        'M': 'TM',
        'S': 'MSS'
        }
SENSOR_COLLECTION_2_RE = '(?P<sensor>[C|O|T|E|M|S])'

SATELLITES = {
        '01': 'Landsat 1',
        '04': 'Landsat 4',
        '05': 'Landsat 5',
        '07': 'Landsat 7',
        '08': 'Landsat 8'
        }
SATELLITE_PRECOLLECTION_RE = '(?P<satellite>[14578])'
SATELLITE_COLLECTION_1_RE = '(?P<satellite>0[14578])'
SATELLITE_COLLECTION_2_RE = '(?P<satellite>0[145789])'
SATELLITE_RE = '(?P<satellite>0[145789])'

PROCESSING_CORRECTION_LEVEL_1_RE = 'L(?P<sensor>[C|O|T|E|M|S])(?P<satellite>0[145789])(?P<delimiter>_)(?P<processing_correction_level>L1)(?P<processing_software>TP|GT|GS|SP)(?P=delimiter)(?P<path>[012][0-9][0-9])(?P<row>[01][0-9][0-9]|2[0-4][0-3])(?P=delimiter)(?P<acquisition_year>19|20\d\d)(?P<acquisition_month>0[1-9]|1[012])(?P<acquisition_day>0[1-9]|[12][0-9]|3[01])(?P=delimiter)(?P<processing_year>19|20\d\d)(?P<processing_month>0[1-9]|1[012])(?P<processing_day>0[1-9]|[12][0-9]|3[01])(?P=delimiter)(?P<collection>0[12])(?P=delimiter)(?P<category>RT|T[1|2])'
PROCESSING_CORRECTION_LEVEL_2_RE = 'L(?P<sensor>[C|O|T|E|M|S])(?P<satellite>0[145789])(?P<delimiter>_)(?P<processing_correction_level>L2)(?P<processing_software>TP|GT|GS|SP)(?P=delimiter)(?P<path>[012][0-9][0-9])(?P<row>[01][0-9][0-9]|2[0-4][0-3])(?P=delimiter)(?P<acquisition_year>19|20\d\d)(?P<acquisition_month>0[1-9]|1[012])(?P<acquisition_day>0[1-9]|[12][0-9]|3[01])(?P=delimiter)(?P<processing_year>19|20\d\d)(?P<processing_month>0[1-9]|1[012])(?P<processing_day>0[1-9]|[12][0-9]|3[01])(?P=delimiter)(?P<collection>0[12])(?P=delimiter)(?P<category>RT|T[1|2])'
PROCESSING_CORRECTION_LEVELS = {
        'L1TP': 'L1TP',
        'L1GT': 'L1GT',
        'L1GS': 'L1GS',
        'L2SP': 'L2SP',
        'L2SR': 'L2SR',
        }
PROCESSING_CORRECTION_LEVEL_RE = '(?P<processing_correction_level>(L[1|2](?:TP|GT|GS|SP)))'
WRS_PATH_ROW_RE = '(?P<path>[012][0-9][0-9])(?P<row>[01][0-9][0-9]|2[0-4][0-3])'
ACQUISITION_YEAR = '(?P<acquisition_year>19|20\\d\\d)'
ACQUISITION_MONTH = '(?P<acquisition_month>0[1-9]|1[012])'
ACQUISITION_DAY = '(?P<acquisition_day>0[1-9]|[12][0-9]|3[01])'
JULIAN_DAY = '(?P<julian_day>[0-2][0-9][0-9]|3[0-6][0-6])'
GROUND_STATION_IDENTIFIER = '(?P<ground_station_identifier>[A-Z][A-Z][A-Z][0-9][0-9])'
PROCESSING_YEAR = '(?P<processing_year>19|20\\d\\d)'
PROCESSING_MONTH = '(?P<processing_month>0[1-9]|1[012])'
PROCESSING_DAY = '(?P<processing_day>0[1-9]|[12][0-9]|3[01])'
COLLECTION_NUMBERS = {
        '01': '01',
        '02': '02'
        }
COLLECTION_NUMBER_RE = '(?P<collection>0[12])'
COLLECTION_1_RE = '(?P<collection>01)'
COLLECTION_2_RE = '(?P<collection>02)'
COLLECTION_CATEGORIES = {
        'RT': 'Real-Time',
        'T1': 'Tier 1',
        'T2': 'Tier 2'
        }
COLLECTION_CATEGORY_RE = '(?P<category>RT|T[1|2])'

MSS123 = {
        'all': (4, 5, 6, 7),
        'visible': (4, 5),
        'infrared': (6, 7)
        }
MSS45 = {
        'all': (1, 2, 3, 4),
        'visible': (1, 2),
        'ndvi': (2, 3),
        'infrared': (3, 4)
        }
LANDSAT_BANDS_ = {
        'oli/tirs': {
            'all': [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 'QA'],
            'oli': [1, 2, 3, 4, 5, 6, 7, 8, 9],
            'tirs': [10, 11],
            'visible': [1, 2, 3, 4],
            'ndvi': [4,5],
            'infrared': [5, 6, 7],
            'shortwave': [6, 7],
            'panchromatic': 8,
            'bqa': ['QA']
            },
        'etm+': {
            'all': (1, 2, 3, 4, 5, 6, 7, 8),
            'visible': (1, 2, 3),
            'ndvi': (3, 4),
            'infrared': (4, 5, 7),
            'shortwave': (5, 7),
            'tirs': 6,
            'panchromatic': 8
            },
        'tm': {
            'all': (1, 2, 3, 4, 5, 6, 7),
            'visible': (1, 2, 3),
            'ndvi': (3, 4),
            'infrared': (4, 5, 7),
            'tirs': 6
            },
        'mss1': MSS123,
        'mss2': MSS123,
        'mss3': MSS123,
        'mss4': MSS45,
        'mss5': MSS45
        }
LANDSAT_BANDS = {
        'all': [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 'QA'],
        'bqa': ['QA'],
        'oli': [1, 2, 3, 4, 5, 6, 7, 8, 9],
        'tirs': [10, 11],
        'coastal': 1,
        'visible': [2, 3, 4],
        'ndvi': [4,5],
        'ndsi': [3,6],
        'infrared': [5, 6, 7, 9],
        'panchromatic': 8
        }
BAND_PRECOLLECTION_RE = '[0-9Q][01A]?'
BAND_RE = '[0-9Q][01A]?'
BAND_RE_TEMPLATE = '(?P<band>B{band_pattern})'
GEOTIFF_EXTENSION = '.TIF'

# Collection 2, Level 2: Landsat Product ID, file type, and extension make the file name
# Example: LC08_L2SP_201031_20190616_20200828_02_T1_SR_B1.TIF

LEVEL_2_FILETYPE_RE_TEMPLATE = '(?P<filetype>{file_type_pattern})'
BAND_LEVEL_2_RE = '[0-9Q][01A]?'

PROCESSING_CORRECTION_LEVEL_RE = '(?P<processing_correction_level>L[12])'
PROCESSING_SOFTWARE_RE = '(?P<processing_software>TP|GT|GS|SP)'
YEAR_RE = '(?P<year>19|20\d\d)'
MONTH_RE = '(?P<month>0[1-9]|1[012])'
DAY_RE = '(?P<day>0[1-9]|[12][0-9]|3[01])'
YEAR_MONTH_DAY_RE_GROUP = '(?P=year)?(?P=month)?(?P=day)?'
COLLECTION_NUMBER_RE = '(?P<collection>0[1|2])'
COLLECTION_CATEGORY_RE = '(?P<category>RT|T[1|2])'
FILETYPE_RE = '(?P<filetype>QA|SR|ST)'
IMAGE_LEVEL_2_RE = '(?P<band_level_2>PIXEL|RADSAT|B[1234567]|QA_AEROSOL|ATRAN|B10|CDIST|[D|T|U]RAD|EM[IS][SD]|QA)'
BAND_LEVEL_2_RE = '(?P<band_level_2>B[1234567]|QA_AEROSOL|B10|QA)'
BAND_LEVEL_2_RE_TEMPLATE = '(?P<band_level_2>B{band_pattern}|QA_AEROSOL|QA)'
IMAGE_LEVEL_2_RE_TEMPLATE = '(?P<image_level_2>{band_pattern})'
# LEVEL_2_FILETYPE_RE_TEMPLATE = '(?P<filetype>{file_type_pattern})'
LEVEL_2_IMAGES = {
    'SR_B1': 'Surface Reflectance Band 1',
    'SR_B2': 'Surface Reflectance Band 2',
    'SR_B3': 'Surface Reflectance Band 3',
    'SR_B4': 'Surface Reflectance Band 4',
    'SR_B5': 'Surface Reflectance Band 5',
    'SR_B6': 'Surface Reflectance Band 6',
    'SR_B7': 'Surface Reflectance Band 7',
    'ST_B10': 'Surface Temperature Band 10',
    'SR_QA_AEROSOL': 'Aerosol retrieval per pixel',
    'QA_PIXEL': 'Level-1 QA Band',
    'QA_RADSAT': 'Radiometric saturation per pixel',
    'ST_QA': 'Surface Temperature QA',
    'ST_TRAD': 'Thermal band converted to radiance',
    'ST_URAD': 'Upwelled Radiance',
    'ST_DRAD': 'Downwelled Radiance',
    'ST_ATRAN': 'Atmospheric Transmittance',
    'ST_EMIS': 'Emissivity estimated from ASTER GED Band 10',
    'ST_EMSD': 'Emissivity standard deviation',
    'ST_CDIST': 'Pixel distance to cloud',
    }

PRECOLLECTION_SCENE_ID = LANDSAT_PREFIX \
        + SENSOR_PRECOLLECTION_RE \
        + SATELLITE_PRECOLLECTION_RE \
        + WRS_PATH_ROW_RE \
        + ACQUISITION_YEAR \
        + JULIAN_DAY \
        + GROUND_STATION_IDENTIFIER
PRECOLLECTION_BAND_TEMPLATE = \
        PRECOLLECTION_SCENE_ID \
        + DELIMITER \
        + BAND_RE_TEMPLATE \
        + GEOTIFF_EXTENSION

COLLECTION_1_SCENE_ID = LANDSAT_PREFIX \
        + SENSOR_COLLECTION_1_RE \
        + SATELLITE_COLLECTION_1_RE \
        + DELIMITER_RE \
        + PROCESSING_CORRECTION_LEVEL_RE \
        + DELIMITER_RE_GROUP \
        + WRS_PATH_ROW_RE \
        + DELIMITER_RE_GROUP \
        + ACQUISITION_YEAR \
        + ACQUISITION_MONTH \
        + ACQUISITION_DAY \
        + DELIMITER_RE_GROUP \
        + PROCESSING_YEAR \
        + PROCESSING_MONTH \
        + PROCESSING_DAY \
        + DELIMITER_RE_GROUP \
        + COLLECTION_1_RE \
        + DELIMITER_RE_GROUP \
        + COLLECTION_CATEGORY_RE
COLLECTION_1_BAND_TEMPLATE = \
        COLLECTION_1_SCENE_ID \
        + DELIMITER_RE_GROUP \
        + BAND_RE_TEMPLATE \
        + GEOTIFF_EXTENSION

COLLECTION_2_SCENE_ID = LANDSAT_PREFIX \
        + SENSOR_COLLECTION_2_RE \
        + SATELLITE_COLLECTION_2_RE \
        + DELIMITER_RE \
        + PROCESSING_CORRECTION_LEVEL_RE \
        + PROCESSING_SOFTWARE_RE \
        + DELIMITER_RE_GROUP \
        + WRS_PATH_ROW_RE \
        + DELIMITER_RE_GROUP \
        + ACQUISITION_YEAR \
        + ACQUISITION_MONTH \
        + ACQUISITION_DAY \
        + DELIMITER_RE_GROUP \
        + PROCESSING_YEAR \
        + PROCESSING_MONTH \
        + PROCESSING_DAY \
        + DELIMITER_RE_GROUP \
        + COLLECTION_2_RE \
        + DELIMITER_RE_GROUP \
        + COLLECTION_CATEGORY_RE
COLLECTION_2_LEVEL_1_BAND_TEMPLATE = \
        COLLECTION_2_SCENE_ID \
        + DELIMITER_RE_GROUP \
        + BAND_LEVEL_2_RE_TEMPLATE \
        + GEOTIFF_EXTENSION

COLLECTION_2_LEVEL_1_SCENE_ID = LANDSAT_PREFIX \
        + SENSOR_COLLECTION_2_RE \
        + SATELLITE_COLLECTION_2_RE \
        + DELIMITER_RE \
        + PROCESSING_CORRECTION_LEVEL_RE \
        + PROCESSING_SOFTWARE_RE \
        + DELIMITER_RE_GROUP \
        + WRS_PATH_ROW_RE \
        + DELIMITER_RE_GROUP \
        + ACQUISITION_YEAR \
        + ACQUISITION_MONTH \
        + ACQUISITION_DAY \
        + DELIMITER_RE_GROUP \
        + PROCESSING_YEAR \
        + PROCESSING_MONTH \
        + PROCESSING_DAY \
        + DELIMITER_RE_GROUP \
        + COLLECTION_2_RE \
        + DELIMITER_RE_GROUP \
        + COLLECTION_CATEGORY_RE

COLLECTION_2_LEVEL_2_SCENE_ID = LANDSAT_PREFIX \
        + SENSOR_COLLECTION_2_RE \
        + SATELLITE_COLLECTION_2_RE \
        + DELIMITER_RE \
        + PROCESSING_CORRECTION_LEVEL_RE \
        + PROCESSING_SOFTWARE_RE \
        + DELIMITER_RE_GROUP \
        + WRS_PATH_ROW_RE \
        + DELIMITER_RE_GROUP \
        + ACQUISITION_YEAR \
        + ACQUISITION_MONTH \
        + ACQUISITION_DAY \
        + DELIMITER_RE_GROUP \
        + PROCESSING_YEAR \
        + PROCESSING_MONTH \
        + PROCESSING_DAY \
        + DELIMITER_RE_GROUP \
        + COLLECTION_2_RE \
        + DELIMITER_RE_GROUP \
        + COLLECTION_CATEGORY_RE
COLLECTION_2_LEVEL_2_IMAGE_TEMPLATE = \
        COLLECTION_2_LEVEL_2_SCENE_ID \
        + DELIMITER_RE_GROUP \
        + IMAGE_LEVEL_2_RE_TEMPLATE \
        + GEOTIFF_EXTENSION

LANDSAT_IDENTIFIERS = {
        'prefix': LANDSAT_PREFIX,
        'sensor': {
            'description': 'Sensor',
            'identifiers': {
                'Collection 1': SENSORS_COLLECTION_1,
                'Collection 2': SENSORS_COLLECTION_2,
                'Pre-Collection': SENSORS_PRECOLLECTION,
                },
            'regular_expression': {
                'Collection 1': SENSOR_COLLECTION_1_RE,
                'Collection 2': SENSOR_COLLECTION_2_RE,
                'Pre-Collection': SENSOR_PRECOLLECTION_RE,
                }
            },
        'satellite': {
            'description': 'Satellite',
            'identifiers': SATELLITES,
            'regular_expression': {
                'Collection 1': SATELLITE_COLLECTION_1_RE,
                'Collection 2': SATELLITE_COLLECTION_2_RE,
                'Pre-Collection': SATELLITE_PRECOLLECTION_RE,
                }
            },
        'processing_correction_level': {
            'description': 'Processing correction level',
            'identifiers': PROCESSING_CORRECTION_LEVELS,
            'regular_expression': PROCESSING_CORRECTION_LEVEL_RE
            },
        'processing_correction_level_template': {
                'Level 1': PROCESSING_CORRECTION_LEVEL_1_RE,
                'Level 2': PROCESSING_CORRECTION_LEVEL_2_RE,
                },
        'wrs_path_row': {
            'description': 'WRS Path and Row',
            'regular_expression': WRS_PATH_ROW_RE,
                },
        'acquisition_date': {
            'description': 'Acquisition date',
            'regular_expression': {
                'year': ACQUISITION_YEAR,
                'month': ACQUISITION_MONTH,
                'day': ACQUISITION_DAY,
                'julian_day': JULIAN_DAY
                },
            },
        'ground_station_identifier': GROUND_STATION_IDENTIFIER,
        'processing_date': {
            'description': 'Processing date',
            'regular_expression': {
                'year': PROCESSING_YEAR,
                'month': PROCESSING_MONTH,
                'day': PROCESSING_DAY
                }
            },
        'collection': {
            'description': 'Collection number',
            'identifiers' : COLLECTION_NUMBERS,
            'regular_expression': {
                'collection 1': COLLECTION_1_RE,
                'collection 2': COLLECTION_2_RE,
                }
            },
        'category': {
            'description': 'Collection category',
            'identifiers': COLLECTION_CATEGORIES,
            'regular_expression': COLLECTION_CATEGORY_RE
            },
        'scene_template': {
            'Collection 1': COLLECTION_1_SCENE_ID,
            'Collection 2': COLLECTION_2_SCENE_ID,
            'Pre-Collection': PRECOLLECTION_SCENE_ID,
            },
        COLLECTION_1_SCENE_ID: 'Collection 1',
        COLLECTION_2_SCENE_ID: 'Collection 2',
        PRECOLLECTION_SCENE_ID: 'Pre-Collection',
        'band_template': {
                'Collection 1': COLLECTION_1_BAND_TEMPLATE,
                'Collection 2': {
                    'Level 1': COLLECTION_2_LEVEL_1_BAND_TEMPLATE,
                    'Level 2': COLLECTION_2_LEVEL_2_IMAGE_TEMPLATE,
                    },
                'Pre-Collection': PRECOLLECTION_BAND_TEMPLATE
                },
        COLLECTION_1_BAND_TEMPLATE: 'Collection 1',
        COLLECTION_2_LEVEL_1_BAND_TEMPLATE: 'Collection 2, Level 1',
        COLLECTION_2_LEVEL_2_IMAGE_TEMPLATE: 'Collection 2, Level 2',
        PRECOLLECTION_BAND_TEMPLATE: 'Pre-Collection'
        }
